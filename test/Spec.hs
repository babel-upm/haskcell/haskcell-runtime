import Test.Hspec
import Algebra.Graph.AdjacencyMap hiding (empty)
import Data.Index
import Lib.Dependency

main :: IO ()
main = hspec $ do
         describe "Lib.Dependency.addDependency" $ do
                 it "adds a dependency" $ do
                   addDependency (indexNat 0 0) (indexNat 0 1) mempty `shouldBe` edge (indexNat 0 0) (indexNat 0 1)
