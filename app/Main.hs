{-# LANGUAGE OverloadedStrings #-}

-- | This module contains the main program of the project.
module Main where

-- external imports
import Control.Concurrent (MVar, newMVar, readMVar)
import Control.Monad (forever)
import Control.Monad.Trans.Except (runExceptT)
import Data.Aeson (decode)
import Data.Maybe (fromMaybe)
import qualified Network.WebSockets as WS

-- internal imports

import Data.Cell (Cell (Cell, content, evalResult), getCell, getIndex)
import Data.ExternalModule (ExternalModule)
import Data.Index (indexToRef)
import Data.Messages (Load (..), Save (..))
import Data.ServerState (ServerState, newServerState)
import Lib.Application (
    eval,
    load,
    save,
    send,
    update,
 )
import Lib.Dependency (getDependents, getOrder)
import Lib.ExternalModule (
    saveAndLoadExternalModule,
    saveExternalModuleFile,
 )

-- debug

import Control.Concurrent.Extra (modifyMVar_)
import Data.Module (Module (Module), ModuleContent (ModuleContent))
import Debug.Trace (traceIO)
import Lib.Module (saveAndLoadModule)
import Lib.ServerState
import System.Environment (getArgs)

main :: IO ()
main = do
    port <- getWSPortWithDefault 9160
    let host = "0.0.0.0"
    putStrLn $ "[INFO]: Starting Server at  " ++ host ++ ":" ++ (show port)
    state <- newMVar newServerState
    saveExternalModuleFile ""
    writeFile "CellDefinitions.hs" "module CellDefinitions where\n"
    putStrLn "Done!"
    WS.runServer host port $ application state

getWSPortWithDefault :: Int -> IO Int
getWSPortWithDefault port = do
    args <- findNamedArg "--port" <$> getArgs
    case args of
        (Just str_port) -> return $ read str_port
        Nothing -> return port

findNamedArg :: String -> [String] -> Maybe String
findNamedArg name (name' : (value : args)) = if name == name' then Just value else findNamedArg name (name' : args)
findNamedArg _ _ = Nothing

application :: MVar ServerState -> WS.ServerApp
application state pending = do
    traceIO "[INFO]: Connection started!"
    conn <- WS.acceptRequest pending
    forever $ do
        msg <- WS.receiveData conn

        case decode msg :: Maybe Cell of
            Nothing -> return ()
            Just Cell{content = Nothing} -> return ()
            Just Cell{content = Just ""} -> return ()
            Just cell -> updateApplication state conn cell

        case decode msg :: Maybe Save of
            Nothing -> return ()
            Just (Save "") -> return ()
            Just (Save x) -> save state x

        case decode msg :: Maybe Load of
            Nothing -> return ()
            Just (Load "") -> return ()
            Just (Load extMod) -> loadApplication state conn extMod

        case decode msg :: Maybe ExternalModule of
            Nothing -> return ()
            (Just extMod) -> updateExternalModule state conn extMod

updateApplication :: MVar ServerState -> WS.Connection -> Cell -> IO ()
updateApplication state conn cell = do
    traceIO $ "[INFO]: received cell: " <> show cell
    updateResult <- runExceptT $ update state cell
    s@(ss, deps) <- readMVar state
    result <- case updateResult of
        (Left _) -> do
            let m = ModuleContent $ serverStateToSource s
            cell' <- eval state cell
            modifyMVar_ state $ updateCell cell'
            _ <- runExceptT $ saveAndLoadModule $ Module "CellDefinitions.hs" m
            let updateError = \c -> c{evalResult = Left $ "error evaluating cell " <> indexToRef (getIndex c)}
            let dependents = fmap (updateError . flip getCell ss) (getDependents (getIndex cell) deps)
            return (cell' : dependents)
        (Right _) -> do
            let dependents = fmap (`getCell` ss) (getDependents (getIndex cell) deps)
            mapM (eval state) (cell : dependents)
    mapM_ (send conn) result
    traceIO "[INFO]: state updated!"

loadApplication :: MVar ServerState -> WS.Connection -> String -> IO ()
loadApplication state conn path = do
    traceIO $ "[INFO]: loading file: " <> path
    ext_mod <- load state path
    (ss, deps) <- readMVar state
    let cells = fmap (`getCell` ss) (fromMaybe [] (getOrder deps))
    result <- mapM (eval state) cells
    mapM_ (send conn) result
    send conn ext_mod
    traceIO "[INFO]: file loaded!"

updateExternalModule :: MVar ServerState -> WS.Connection -> ExternalModule -> IO ()
updateExternalModule state conn extMod = do
    traceIO "[INFO]: received external module"
    _ <- runExceptT $ saveAndLoadExternalModule extMod
    (ss, deps) <- readMVar state
    let cells = fmap (`getCell` ss) (fromMaybe [] (getOrder deps))
    result <- mapM (eval state) cells
    mapM_ (send conn) result
    traceIO "[INFO]: external module updated!"
