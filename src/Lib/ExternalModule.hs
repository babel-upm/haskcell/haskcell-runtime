{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

-- |This module contains a library of functions to deal with the @ExternalModule@
module Lib.ExternalModule where

import NeatInterpolation (untrimming)

-- external imports

import Control.Exception (try)
import Control.Monad.Trans.Except (ExceptT (..))
import Data.Cell (Error)
import Data.Either.Combinators (mapLeft)
import Data.ExternalModule (ExternalModule (..))
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Language.Haskell.Interpreter as I (
    MonadIO (liftIO),
    loadModules,
    runInterpreter,
 )

{- |'saveAndLoadExternalModule' first saves the content of the @ExternalModule@ using
 'saveExternalModuleFile', then checks if compiles loading it on a GHC interpreter session.
-}
saveAndLoadExternalModule :: ExternalModule -> ExceptT Error IO ()
saveAndLoadExternalModule (ExternalModule input) = ExceptT $ do
    liftIO $ saveExternalModuleFile input
    res <- liftIO $ runInterpreter $ I.loadModules ["ExternalModule.hs", "src/TH/Defaults/Signatures.hs"]
    return . mapLeft (const "Won't compile") $ res

{- |'saveExternalModuleFile' writes a file with the content of the @ExternalModule@. This file is
 called @ExternalModule.hs@ and is located in the path where the program is executed.
-}

-- saveExternalModuleFile :: String -> IO ()
saveExternalModuleFile input = do
    let input' = Text.unlines . map ("  " <>) . Text.lines $ Text.pack input
    Text.writeFile "ExternalModule.hs" (header <> "signatures \n  [d|\n" <> input' <> "\n  |]")

header =
    [untrimming|
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module ExternalModule where

import TH.Defaults.Arguments
import TH.Defaults.Expressions
import TH.Defaults.Signatures

|]
