-- | This module contains functions to deal with parsing of a cell content.
module Lib.Parsing (
    toLowercaseVariables,

    -- * Reference Functions
    parseReferences,

    -- ** Auxiliar Reference Functions
    matchReferences,

    -- ** Desugar Functions
    desugarContent,
    desugarExpression,
    desugarRange,
    desugarList,

    -- ** Auxiliar Desugar Functions
    desugar,
    genRange,
    rangeToIndex,
    toListString,
    toRangeString,
    showRefs,
) where

-- external imports

import Data.List.Extra (lower)
import Data.Maybe (catMaybes, isNothing, mapMaybe)
import Data.String.Utils (replace)
import Text.Regex (Regex, matchRegexAll)

-- internal imports
import Data.Cell (Error, Index)
import qualified Data.Index as Index
import Data.Parsing (listRegex, rangeRegex, referencesRegex)

-- | 'parseReferences' obtains all the indices from the references of an input @String@.
parseReferences :: String -> [Index]
parseReferences = mapMaybe Index.rowColToInt . mapMaybe Index.obtainRowCol . matchReferences

{- | 'matchReferences' obtains all the matching references in the
 input.
-}
matchReferences :: String -> [String]
matchReferences = maybe [] f . matchRegexAll referencesRegex
  where
    f (_, x, xs, _) = x : matchReferences xs

{- | 'desugarContent' parses the input, checks that indices are correct and desugar them and changes
 the ranges of values. An example:

 >>> desugarContent "(A0:B2)"
 Right "(A0 A1 A2 B0 B1 B2)"
 >>> desugarContent "(B2:A0)"
 Left "Incorrect index"
-}
desugarContent :: String -> Either Error String
desugarContent s = do
    res <- desugarRange s
    desugarList res

{- | 'desugarRange' is a specialized version of the 'desugar' function to deal with ranges.

 >>> desugarRange "(A1:B2)"
 Right "(A1 A2 B1 B2)"
 >>> desugarRange "[A1:B2]"
 Right "[A1:B2]"
 >>> desugarRange "(A1:B0)"
 Left "Incorrect index"
-}
desugarRange :: String -> Either Error String
desugarRange = desugar rangeRegex toRangeString

{- | 'desugarList' is a specialized version of the 'desugar' function to deal with lists.

 >>> desugarList "[A0:B3]"
 Right "[A0,A1,A2,A3,B0,B1,B2,B3]"
 >>> desugarList "[B3:A0]"
 Left "Incorrect index"
-}
desugarList :: String -> Either Error String
desugarList = desugar listRegex toListString

{- | 'desugarList' is a specialized version of the 'desugar' function to deal with lists.

 >>> desugarExpression "[A0,A1,A2,A3,B0,B1,B2,B3]"
 Right "[a0,a1,a2,a3,b0,b1,b2,b3]"
-}
desugarExpression :: String -> Either Error String
desugarExpression = desugarReference referencesRegex toLowercaseVariables

{- | 'desugar' is a higher order function that matches a regex with the input string generates the
 ranges and finally returns the desugarized string.
-}
desugar ::
    Regex ->
    (String -> [Index] -> String -> String) ->
    String ->
    Either Error String
desugar regex f s
    | isNothing res = Right s
    | otherwise = do
        let Just (pre, matched, post, _) = res
        is <- rangeToIndex matched
        rs <- uncurry genRange is
        post' <- desugar regex f post
        return $ f pre rs post'
  where
    res = matchRegexAll regex s

desugarReference ::
    Regex ->
    (String -> [Index] -> String -> String) ->
    String ->
    Either Error String
desugarReference regex f s
    | isNothing res = Right s
    | otherwise = do
        let Just (pre, matched, post, _) = res
        let is = parseReferences matched
        post' <- desugarReference regex f post
        return $ f pre is post'
  where
    res = matchRegexAll regex s

{- | 'genRange' returns the range between two given indices.

 >>> genRange (0,0) (2,2)
 Right [(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)]
 >>> genRange (2,2) (0,0)
 Left "Incorrect index"
-}
genRange :: Index -> Index -> Either Error [Index]
genRange idxA idxB
    | Index.col idxA <= Index.col idxB && Index.row idxA <= Index.row idxB =
        Right $
            Index.index
                <$> [(Index.col idxA) .. (Index.col idxB)]
                <*> [Index.row idxA .. Index.row idxB]
    | otherwise = Left "Incorrect index"

{- | 'rangeToIndex' translates a range to its indices.

 >>> rangeToIndex "(A1:B2)"
 Right ((1,0),(2,1))
 >>> rangeToIndex "(A1:)"
 Left "Cannot parse index"
-}
rangeToIndex :: String -> Either Error (Index, Index)
rangeToIndex = toTuple . parseReferences
  where
    toTuple [x, y] = Right (x, y)
    toTuple _ = Left "Cannot parse index"

-- | 'toListString'
toListString :: String -> [Index] -> String -> String
toListString pre xs post = pre <> "[" <> replace " " "," (showRefs xs) <> "]" <> post

-- | 'toRangeString'
toRangeString :: String -> [Index] -> String -> String
toRangeString pre xs post = pre <> showRefs xs <> post

toLowercaseVariables :: String -> [Index] -> String -> String
toLowercaseVariables pre xs post = pre <> lower (showRefs xs) <> post

-- | 'showRefs'
showRefs :: [Index] -> String
showRefs = unwords . map Index.indexToRef
