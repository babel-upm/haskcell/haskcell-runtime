{-# LANGUAGE OverloadedStrings #-}

-- | This module contains a library of functions used by the main application of the project
module Lib.Application where

-- external imports
import Control.Concurrent (MVar, modifyMVar_, readMVar)
import Control.Monad (guard, unless)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Trans.Except (ExceptT, except, runExceptT)
import Control.Monad.Trans.Maybe (MaybeT (MaybeT, runMaybeT), maybeToExceptT)
import Data.Aeson (
    ToJSON,
    eitherDecodeFileStrict',
    encode,
    encodeFile,
 )
import Data.List.Extra (lower)
import Data.Maybe (fromMaybe, isJust)
import qualified Network.WebSockets as WS

-- internal imports
import Data.Cell (Cell (Cell, content, evalResult), Error, Index, getCell, getIndex)
import Data.ExternalModule (ExternalModule (..))
import Data.Index (indexToRef)
import Data.Module (Module (Module), ModuleContent (ModuleContent))
import Data.ServerState (ServerState)
import Data.SpreadSheet as SS (toListValues)
import Lib.Dependency (
    addDependencies,
    circularDependencies,
    getDependencies,
    getDependents,
    getOrder,
 )
import qualified Lib.Eval as Eval
import Lib.ExternalModule (saveAndLoadExternalModule)
import Lib.Module (saveAndLoadModule)
import Lib.Parsing (desugarContent, parseReferences)
import Lib.ServerState (
    serverStateToSource,
    updateCell,
    updateState,
 )
import System.Directory (doesFileExist)

-- debug
import Debug.Trace (traceIO)

-- | 'update' updates the state of the server and the 'CellDefinitions' file with the given cell.
update :: MVar ServerState -> Cell -> ExceptT Error IO ()
update _ cell@Cell{content = Nothing} = return ()
update state cell@Cell{content = Just c} = do
    -- we store the content of the expression in the state
    liftIO $ modifyMVar_ state $ updateCell cell
    -- we analyze the dependencies
    deps <- analyzeDependencies state cell
    liftIO $ updateState state cell deps
    state <- liftIO $ readMVar state
    -- we update the cell definitions module
    let m = ModuleContent $ serverStateToSource state
    saveAndLoadModule $ Module "CellDefinitions.hs" m

-- | 'eval' evaluates a cell and returns a new cell with the result inside the 'evalResult' field.
eval :: MVar ServerState -> Cell -> IO Cell
eval _ cell@Cell{content = Nothing} = return cell
eval _ cell@Cell{content = Just ""} = return cell
eval state cell = do
    let variableName = lower . indexToRef $ getIndex cell
    result <- runExceptT $ Eval.eval variableName
    return $ cell{evalResult = result}

-- | 'save' saves the actual state of the server in the received path.
save :: MVar ServerState -> String -> IO ()
save state path = do
    s <- readMVar state
    file <- readFile "ExternalModule.hs" -- unsafe
    encodeFile path (s, ExternalModule file)

-- | 'load' loads the state saved in the file received as argument.
load :: MVar ServerState -> String -> IO ExternalModule
load state path = do
    -- we check if the file exists
    exists <- doesFileExist path
    unless exists $ liftIO $ traceIO $ "[ERROR]: file: '" <> path <> "' not found."
    -- if exists we load it
    guard exists
    Right (s, ext_mod) <- eitherDecodeFileStrict' path :: IO (Either Error (ServerState, ExternalModule))
    -- if it is loaded modifies the state with the read one
    modifyMVar_ state $ \_ -> return s
    runExceptT $ saveAndLoadExternalModule ext_mod
    let cell_defs = Module "CellDefinitions.hs" (ModuleContent $ serverStateToSource s)
    runExceptT $ saveAndLoadModule cell_defs
    return ext_mod

-- | 'analyzeDependencies' looks for circular references.
analyzeDependencies :: MVar ServerState -> Cell -> ExceptT Error IO (Index, [Index])
analyzeDependencies _ cell@Cell{content = Nothing} = pure (getIndex cell, [])
analyzeDependencies state cell@Cell{content = Just c} = do
    c <- except $ desugarContent c
    (_, deps) <- liftIO $ readMVar state
    let i = getIndex cell
        refs = parseReferences c
    except $
        if circularDependencies $ addDependencies i refs deps
            then Left "Circular dependencies found"
            else Right (i, refs)

-- | 'send' encodes and sends data that can be converted to JSON through an open WebSocket connection.
send :: ToJSON a => WS.Connection -> a -> IO ()
send conn = WS.sendTextData conn . encode
