module Lib.CodeGen where

import Data.Cell (Cell (col, content, row))
import Data.Maybe (fromMaybe)

genSpreadSheet :: String
genSpreadSheet = "empty"

addCellCode :: Cell -> String
addCellCode cell = " & putCell " <> "(" <> r <> "," <> c <> ") " <> "(const $ " <> ct <> ")"
  where
    r = show $ row cell
    c = show $ col cell
    ct = fromMaybe "" $ content cell

genSpreadSheetAndFillWithCells :: [Cell] -> String
genSpreadSheetAndFillWithCells cs = genSpreadSheet <> foldr (mappend . addCellCode) "" cs
