{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

{- | This module contains a library of functions to deal with the
 @ServerState@
-}
module Lib.ServerState where

-- external imports
import Control.Concurrent (MVar, modifyMVar_)
import NeatInterpolation (untrimming)

-- internal imports
import Data.Cell (Cell (Cell, content), Error, Index, addCell, evalResult)
import Data.Index (index, indexToRef)
import Data.List.Extra (lower)
import Data.ServerState (ServerState)
import Data.SpreadSheet (SpreadSheet)
import qualified Data.SpreadSheet as SS
import Data.Text (Text)
import qualified Data.Text as Text
import Lib.Dependency (updateDependency)
import Lib.Parsing (desugarContent, desugarExpression)

-- | 'updateState' updates the state of the server with the given cell and its dependencies.
updateState :: MVar ServerState -> Cell -> (Index, [Index]) -> IO ()
updateState state cell deps = do
    modifyMVar_ state $ updateCell cell
    modifyMVar_ state $ updateDependencies deps

-- | 'updateDependencies' updates the state of the dependencies of the @ServerState@.
updateDependencies :: Monad m => (Index, [Index]) -> ServerState -> m ServerState
updateDependencies (from, tos) (ss, ds) = return (ss, updateDependency from tos ds)

-- | 'updateCell' updates the @ServerState@ adding the new cell, or replaces it if already exists.
updateCell :: Monad m => Cell -> ServerState -> m ServerState
updateCell cell (ss, ds) = return (addCell cell ss, ds)

header =
    [untrimming|
{-# LANGUAGE TemplateHaskell #-}

module CellDefinitions where

import ExternalModule
import TH.Defaults.Arguments
import TH.Defaults.Expressions
import Data.Map.Strict as Map

|]

serverStateToSource :: ServerState -> Text
serverStateToSource (ss, deps) = header <> ssToSource ss
  where
    ssToSource :: SpreadSheet Cell -> Text
    ssToSource = Text.unlines . fmap cellPosToSource . SS.toListPosValues

    posToVariableName :: Index -> Text
    posToVariableName = Text.pack . lower . indexToRef

    cellPosToSource :: (Index, Cell) -> Text
    cellPosToSource (pos, cell) =
        let desugared = desugar <$> content cell
            tPos = posToVariableName pos
         in case desugared of
                Nothing -> "-- " <> tPos <> " -- BLANK"
                Just (Left e) -> "-- " <> tPos <> " -- ERROR: " <> Text.pack (show e)
                Just (Right "") -> "-- " <> tPos <> " -- BLANK"
                Just (Right c) -> "defaults . arguments $ [d| " <> tPos <> " = " <> Text.pack c <> " |]"

    desugar :: String -> Either Error String
    desugar s = do
        content <- desugarContent s
        desugarExpression content
