-- | This module contains functions to handle the evaluation of the content of cells.
module Lib.Eval (
    -- * Evaluation Functions
    context,
    eval,

    -- * Dependency Solving Functions
    solveDependencies,

    -- ** Auxiliar Functions
    applyValues,
    applyValue,
    cellToIndexAndVal,
) where

-- external imports

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except (ExceptT (..), except)
import Data.Either (lefts, rights)
import Data.SpreadSheet (SpreadSheet)
import Data.String.Utils (replace)
import Language.Haskell.Interpreter (
    InterpreterError (WontCompile),
    InterpreterT,
    MonadIO (liftIO),
    errMsg,
    loadModules,
    runInterpreter,
    setImports,
    typeChecks,
 )
import qualified Language.Haskell.Interpreter as I (eval)
import System.Environment (lookupEnv)

-- internal imports

import Data.Cell (Cell (col, content, row), Error, Index, getCell)
import Data.Index (intToCol, intToRow)
import qualified Data.Index as Index
import Debug.Trace (traceIO)
import Language.Haskell.Interpreter.Unsafe (unsafeRunInterpreterWithArgsLibdir)
import Lib.Parsing (desugarContent)

{- | 'context' configures the modules and the environment of the cell evaluation. The loaded modules
 are:

 - Prelude
 - ExternalModule
 - Data.SpreadSheet
 - Data.SpreadSheet.Cell
 - Data.Function
-}
context :: InterpreterT IO ()
context = do
    loadModules
        [ "ExternalModule.hs"
        , "CellDefinitions.hs"
        , "src/TH/Defaults/Expressions.hs"
        , "src/TH/Defaults/Arguments.hs"
        , "src/TH/Defaults/Signatures.hs"
        ]
    setImports
        [ "Prelude"
        , "ExternalModule"
        , "Data.Function"
        , "Data.Either"
        , "CellDefinitions"
        , "TH.Defaults.Expressions"
        , "TH.Defaults.Arguments"
        , "TH.Defaults.Signatures"
        ]

newtype EvalInterpreterOptions = EvalInterpreterOptions
    { customLibDir :: Maybe FilePath
    }
    deriving (Show, Eq)

defaultEvalInterpreterOptions :: EvalInterpreterOptions
defaultEvalInterpreterOptions =
    EvalInterpreterOptions
        { customLibDir = Nothing
        }

interpreterWithOptions options = case customLibDir options of
    Nothing -> runInterpreter
    Just libDir -> unsafeRunInterpreterWithArgsLibdir [] libDir

{- | 'eval' typechecks and evals the content of a cell.

 >>> runExceptT $ eval "1 + 2"
 Right "3"
 >>> runExceptT $ eval "1 + True"
 Left "Won't compile"
-}
eval :: String -> ExceptT Error IO String
eval "" = return ""
eval input = ExceptT $ do
    libDir <- liftIO $ lookupEnv "NIX_GHC_LIBDIR"
    let interpreter = interpreterWithOptions (defaultEvalInterpreterOptions{customLibDir = libDir})
    typeRes <- liftIO $ interpreter $ do context; typeChecks input
    evalRes <- liftIO $ interpreter $ do context; I.eval input
    return $ case (typeRes, evalRes) of
        (Right True, Left _) -> Left "not showable"
        (_, Left (WontCompile errors)) -> Left $ unlines $ fmap errMsg errors
        (Right True, Right x) -> Right x
        (_, _) -> Left "unknown error"

{- | 'solveDependencies' returns the string with the cell dependencies solved. The @[Index]@ input
 must be in topological order.
-}
solveDependencies :: SpreadSheet Cell -> [Index] -> String -> Either Error String
solveDependencies _ _ "" = Right ""
solveDependencies _ [] xs = Right xs
solveDependencies state is _ = do
    let is' = reverse is -- the last element is the string we want to eval
        vs = fmap (cellToIndexAndVal . flip getCell state) is'
    case lefts vs of
        [] -> Right $ applyValues . rights $ vs
        err -> Left $ foldr (<>) [] err

-- | 'applyValues'
applyValues :: [(String, String)] -> String
applyValues [] = [] -- this case should never happen
applyValues [x] = snd x
applyValues (x : xs) = applyValues $ (fmap . fmap) (applyValue x) xs

-- | 'applyValue'
applyValue :: (String, String) -> String -> String
applyValue (from, to) = replace from to

-- | 'cellToIndexAndVal'
cellToIndexAndVal :: Cell -> Either Error (String, String)
cellToIndexAndVal cell = do
    c <- case content cell of
        Nothing -> Left $ "Error empty cell " <> index
        Just "" -> Left $ "Error empty cell " <> index
        Just x -> Right x
    c' <- desugarContent c
    return (index, "(" <> c' <> ")")
  where
    index = Index.intToCol (Index._col $ col cell) <> Index.intToRow (Index._row $ row cell)
