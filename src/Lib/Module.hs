-- |This module contains a library of functions to deal with the @ExternalModule@
module Lib.Module where

-- external imports

import Control.Exception (try)
import Control.Monad.Trans.Except (ExceptT (..))
import Data.Cell (Error)
import Data.Either.Combinators (mapLeft)
import Data.Module (
    Module (Module),
    ModuleContent (ModuleContent),
    ModuleName (ModuleName),
 )
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Debug.Trace (traceIO)
import Language.Haskell.Interpreter as I (
    MonadIO (liftIO),
    loadModules,
    runInterpreter,
 )

{- |'saveAndLoadModule' first saves the content of the @Module@ using 'saveModuleFile', then checks if
 compiles loading it on a GHC interpreter session.
-}
saveAndLoadModule :: Module -> ExceptT Error IO ()
saveAndLoadModule m@(Module (ModuleName name) (ModuleContent content)) = ExceptT $ do
    liftIO $ saveModuleFile m
    res <- liftIO $ runInterpreter $ I.loadModules [T.unpack name]
    return . mapLeft (const "Won't compile") $ res

{- |'saveModuleFile' writes a file with the content of the @Module@. This file is called @Module.hs@
 and is located in the path where the program is executed.
-}
saveModuleFile :: Module -> IO ()
saveModuleFile (Module (ModuleName name) (ModuleContent content)) = do
    T.writeFile (T.unpack name) content
