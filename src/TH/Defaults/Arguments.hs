{-# LANGUAGE TemplateHaskell #-}

module TH.Defaults.Arguments where

import Language.Haskell.TH
import TH.Defaults.Expressions

import Debug.Trace (trace)

arguments :: Q [Dec] -> Q [Dec]
arguments decs = do
    decs' <- decs
    mapM functionArgsDefaultsDecs decs'

functionArgsDefaultsDecs :: Dec -> Q Dec
functionArgsDefaultsDecs (ValD n b d) = do
    b' <- functionArgsDefaultsBody b
    d' <- mapM functionArgsDefaultsDecs d
    return $ ValD n b' d'
functionArgsDefaultsDecs x = return x

functionArgsDefaultsBody :: Body -> Q Body
functionArgsDefaultsBody (NormalB e) = do
    e' <- functionArgsDefaultsExp e
    return $ NormalB e'
functionArgsDefaultsBody (GuardedB e) = do
    e' <- mapM guardsExp e
    return $ GuardedB e'
  where
    guardsExp (g, e) = do
        e' <- functionArgsDefaultsExp e
        return (g, e')

functionArgsDefaultsExp :: Exp -> Q Exp
-- TODO: ignore expressions without name in x
functionArgsDefaultsExp e@(AppE x y) = do
    op <- [|(?)|]
    let arity = getArity e
        name = nameBase (getName x) <> "_arg_" <> show arity
    n <- lookupValueName name
    case n of
        Just n -> do
            op <- [|(?)|]
            x' <- functionArgsDefaultsExp x
            let arg = InfixE (Just y) op (Just (VarE (mkName name)))
            return $ AppE x' arg
        Nothing -> return e
functionArgsDefaultsExp e = return e

getArity :: Exp -> Integer
getArity (AppE x _) = 1 + getArity x
getArity _ = 0

getName :: Exp -> Name
getName (AppE x _) = getName x
getName (VarE n) = n
getName (ConE n) = n
getName e = error $ "expression " <> show e <> " doesn't contain name"

debug message x = trace (message <> show x) (return ())
