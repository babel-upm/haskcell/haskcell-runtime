{-# LANGUAGE TemplateHaskell #-}

module TH.Defaults.Expressions where

import Language.Haskell.TH

-- (?) :: a -> a -> a
(?) = error "(?) not removed during splice"

defaults :: Q [Dec] -> Q [Dec]
defaults decs = do
    decs' <- decs
    mapM defaultsDec decs'

defaultsExpr :: Exp -> Q Exp
defaultsExpr (InfixE (Just l) op (Just r)) = do
    op' <- [|(?)|]
    case l of
        (UnboundVarE _) | op' == op -> defaultsExpr r
        l | op' == op -> defaultsExpr l
        _ -> do
            l' <- defaultsExpr l
            r' <- defaultsExpr r
            return $ InfixE (Just l') op (Just r')
defaultsExpr (AppE e1 e2) = do
    e1' <- defaultsExpr e1
    e2' <- defaultsExpr e2
    return $ AppE e1' e2'
defaultsExpr (AppTypeE e t) = do
    e' <- defaultsExpr e
    return $ AppTypeE e' t
defaultsExpr (UInfixE l op r) = do
    op' <- [|(?)|]
    case l of
        (UnboundVarE _) | op' == op -> defaultsExpr r
        l | op' == op -> defaultsExpr l
        _ -> do
            l' <- defaultsExpr l
            r' <- defaultsExpr r
            return $ UInfixE l' op r'
defaultsExpr (ParensE e) = do
    e' <- defaultsExpr e
    return $ ParensE e'
defaultsExpr (LamE pats e) = do
    e' <- defaultsExpr e
    pats' <- mapM defaultsPat pats
    return $ LamE pats' e'
defaultsExpr (TupE maybe_exps) = do
    maybe_exps' <- mapM (mapM defaultsExpr) maybe_exps
    return $ TupE maybe_exps'
defaultsExpr (UnboxedTupE maybe_exps) = do
    maybe_exps' <- mapM (mapM defaultsExpr) maybe_exps
    return $ UnboxedTupE maybe_exps'
defaultsExpr (UnboxedSumE e sum_alt sum_ari) = do
    e' <- defaultsExpr e
    return $ UnboxedSumE e' sum_alt sum_ari
defaultsExpr (CondE e1 e2 e3) = do
    e1' <- defaultsExpr e1
    e2' <- defaultsExpr e2
    e3' <- defaultsExpr e3
    return $ CondE e1' e2' e3'
defaultsExpr (MultiIfE guard_exps) = do
    guard_exps' <- mapM defaultsGuard guard_exps
    return $ MultiIfE guard_exps
defaultsExpr (LetE decs e) = do
    decs' <- mapM defaultsDec decs
    e' <- defaultsExpr e
    return $ LetE decs' e'
defaultsExpr (CaseE e ms) = do
    e' <- defaultsExpr e
    return $ CaseE e' ms
defaultsExpr (DoE modName stmts) = do
    stmts' <- mapM defaultsStmt stmts
    return $ DoE modName stmts'
defaultsExpr (MDoE modName stmts) = do
    stmts' <- mapM defaultsStmt stmts
    return $ MDoE modName stmts'
defaultsExpr (CompE stmts) = do
    stmts' <- mapM defaultsStmt stmts
    return $ CompE stmts'
defaultsExpr (ListE es) = do
    es' <- mapM defaultsExpr es
    return $ ListE es'
defaultsExpr (SigE e t) = do
    e' <- defaultsExpr e
    return $ SigE e' t
defaultsExpr (RecConE n f_es) = do
    f_es' <- mapM defaultsFieldExp f_es
    return $ RecConE n f_es'
defaultsExpr (RecUpdE e f_es) = do
    e' <- defaultsExpr e
    f_es' <- mapM defaultsFieldExp f_es
    return $ RecUpdE e' f_es'
defaultsExpr (StaticE e) = do
    e' <- defaultsExpr e
    return $ StaticE e'
defaultsExpr exp = return exp

defaultsGuard :: (Guard, Exp) -> Q (Guard, Exp)
defaultsGuard (g, e) = do
    e' <- defaultsExpr e
    return (g, e')

defaultsFieldExp :: FieldExp -> Q FieldExp
defaultsFieldExp (n, e) = do
    e' <- defaultsExpr e
    return (n, e')

defaultsStmt :: Stmt -> Q Stmt
defaultsStmt (BindS p e) = do
    p' <- defaultsPat p
    e' <- defaultsExpr e
    return $ BindS p' e'
defaultsStmt (LetS decs) = do
    decs' <- mapM defaultsDec decs
    return $ LetS decs'
defaultsStmt (NoBindS e) = do
    e' <- defaultsExpr e
    return $ NoBindS e'
defaultsStmt (ParS stmts) = do
    stmts' <- mapM (mapM defaultsStmt) stmts
    return $ ParS stmts'
defaultsStmt (RecS stmts) = do
    stmts' <- mapM defaultsStmt stmts
    return $ RecS stmts'

defaultsDec :: Dec -> Q Dec
defaultsDec (FunD name clauses) = do
    clauses' <- mapM defaultsClauses clauses
    return $ FunD name clauses'
defaultsDec (ValD pat body decs) = do
    body' <- defaultsBody body
    decs' <- mapM defaultsDec decs
    return $ ValD pat body' decs'
defaultsDec d = return d

defaultsClauses :: Clause -> Q Clause
defaultsClauses (Clause pats body decs) = do
    pats' <- mapM defaultsPat pats
    body' <- defaultsBody body
    decs' <- mapM defaultsDec decs
    return $ Clause pats' body' decs'

defaultsPat :: Pat -> Q Pat
defaultsPat = return

defaultsBody :: Body -> Q Body
defaultsBody (NormalB exp) = do
    exp' <- defaultsExpr exp
    return (NormalB exp')
defaultsBody (GuardedB guards) = do
    guards' <- mapM defaultsGuard guards
    return (GuardedB guards')
  where
    defaultsGuard (guard, exp) = do
        exp' <- defaultsExpr exp
        return (guard, exp')
