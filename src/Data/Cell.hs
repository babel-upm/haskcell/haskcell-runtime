{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module contains the type definitions and instances to deal with Cells.
module Data.Cell (
    -- * Type synonyms
    Row,
    Col,
    Index,
    Error,

    -- * Data type
    Cell (..),

    -- * Lib
    getCell,
    getIndex,
    moveTo,
    addCell,
    clearCell,
) where

-- external imports
import Data.Aeson (FromJSON (parseJSON), ToJSON, withObject, (.:))
import Data.Maybe (fromMaybe)
import GHC.Generics (Generic)
import Numeric.Natural (Natural)

-- internal imports

import Data.Index (Col (Col), Index (Index), Row (Row))
import qualified Data.Index as Index
import Data.SpreadSheet (SpreadSheet)
import qualified Data.SpreadSheet as SpreadSheet

-- Lib data types
type Error = String

data Cell = Cell
    { row :: Row
    , col :: Col
    , content :: Maybe String
    , evalResult :: Either Error String
    }
    deriving (Generic, Read)

instance Show Cell where
    show (Cell r c cont res) =
        "(" <> show r <> ", " <> show c <> "): "
            <> show cont
            <> " "
            <> show res

instance FromJSON Cell where
    parseJSON = withObject "cell" $ \o -> do
        r <- o .: "row"
        c <- o .: "col"
        ct <- o .: "content"
        return $ Cell r c ct (Right "")

instance ToJSON Cell

instance Semigroup Cell where
    (<>) = mergeCell

instance Monoid Cell where
    mempty = Cell (Row 0) (Col 0) Nothing (Right "")

mergeCell :: Cell -> Cell -> Cell
mergeCell Cell{content = Nothing} cell2 = cell2
mergeCell cell1 Cell{content = Nothing} = cell1
mergeCell _ cell2 = cell2

-- | 'getIndex' returns the index of a cell
getIndex :: Cell -> Index
getIndex cell = Index.index (col cell) (row cell)

-- | Modifies cell internal position
moveTo :: Index -> Cell -> Cell
moveTo idx cell = cell{row = Index.row idx, col = Index.col idx}

{- | 'addCell' inserts a cell in a @SpreadSheet@.

 >>> c = Cell 0 0 (Just "1 + 2") (Right "3")
 >>> addCell c empty
 Range (0,0) (0,0)
 fromList [((0,0),(0, 0): Just "1 + 2" Right "3")]
-}
addCell :: Cell -> SpreadSheet Cell -> SpreadSheet Cell
addCell c = SpreadSheet.put (getIndex c) (const c)

-- | 'getCell' returns a @Cell@ in the index of a given @SpreadSheet@.
getCell :: Index -> SpreadSheet Cell -> Cell
getCell i = fromMaybe (moveTo i mempty) . SpreadSheet.get i

{- | 'clearCell' deletes the content of the cell in a given index of a
 @SpreadSheet@

 >>> c = Cell 0 0 (Just "1 + 2") (Right "3")
 >>> clearCell (0,0) $ addCell c empty
 Range (0,0) (0,0)
 fromList [((0,0),(0, 0): Nothing Right "")]
-}
clearCell :: Index -> SpreadSheet Cell -> SpreadSheet Cell
clearCell = addCell . mempty
