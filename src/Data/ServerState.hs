-- | This module contains the definition of the ServerState used by the web server.
module Data.ServerState where

-- internal imports

import Data.Cell (Cell)
import Data.Dependency as Dep (Dependencies, empty)
import Data.SpreadSheet as SS (SpreadSheet, empty)

type ServerState = (SpreadSheet Cell, Dependencies)

-- | The value 'newServerstate' contains the initial value of the state of the server.
newServerState :: ServerState
newServerState = (SS.empty, Dep.empty)
