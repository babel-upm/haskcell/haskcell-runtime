{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Data.SpreadSheet (
    SpreadSheet,

    -- * Constructors
    empty,
    singleton,
    singletonPos,
    fromList,
    toListValues,
    toListPosValues,
    fromDict,

    -- * select and reference
    get,
    selectRange,
    row,
    column,
    select,

    -- * Update/Modify
    put,
    union,
    intersection,
    (</>),
    mapMaybe,

    -- * Properties

    -- width,
    -- height,
    -- limits,
    -- columns,
    -- rows,
) where

import Data.Aeson (FromJSON, ToJSON)
import Data.Bifunctor (bimap)
import Data.Foldable (toList)
import Data.Ix (Ix)
import qualified Data.Ix as Ix
import qualified Data.Map.Strict as Map
import Data.Tuple (swap)
import GHC.Generics
import Numeric.Natural (Natural (..))

-- Internal imports
import Data.Index (Col (Col), Index (..), Row (Row), index)
import qualified Data.Index as Index

{- | Posición de una celda en la hoja de cálculo. Comienzan en @(1,1)@.

 Punto bidimensional en el que la primera coordenada corresponde a
 la columna y la segunda a la fila.

 Ejemplo para localizar una celda:

 +------+----------+
 | @A3@ | @(1,3)@  |
 +------+----------+
 | @J9@ | @(10,9)@ |
 +------+----------+
-}

{- | 'SpreadSheet'  representa la estructura  de datos de una  hoja de
 cálculo.

 Las hojas cálculo pueden contener cualquier tipo de dato, pero solo
 valores de dicho tipo. Es decir, no se podrán tener hojas de
 cálculo heterogéneas que contengan valores 'Double' o 'Bool' a la
 vez. Para una representación más cercana al uso ordinario de las
 hojas de cálculo se recomienda mirar el módulo
 'Data.SpreadSheet.Cell.Cell'.

 La implementación interna de esta estructura de datos es mediante
 un diccionario ('Data.Map.Map' 'Data.Map.Strict') donde las claves son posiciones ('Pos').

 En este módulo se definen un conjunto de operaciones para la
 creación, consulta y modificación de hojas de cálculo.
-}
newtype SpreadSheet a = SpreadSheet
    { getSpreadSheet :: Map.Map Index a
    }
    deriving (Eq, Show, Generic, FromJSON, ToJSON, Functor, Semigroup, Monoid, Foldable)

-- | Properties

{- | Number of columns of an @SpreadSheet@
 width :: SpreadSheet a -> Maybe Natural
 width Empty = Nothing
 width (Mk t b _) = Just $ Index.col b - Index.col t
-}

-- -- | Number of rows of an @Spreadsheet@
-- height :: SpreadSheet a -> Maybe Natural
-- height Empty = Nothing
-- height (Mk t b _) = Just $ Index.row b - Index.row t

-- {- | Indica el rango en el que están contenidos los valores de la hoja
--  de cálculo.
-- -}
-- limits :: SpreadSheet a -> Maybe (Index, Index)
-- limits Empty = Nothing
-- limits (Mk t b _) = Just (tl, br)

-- {- | Enumera las columnas de la hoja de cálculo.

--  >>> columns empty
--  []

--  >>> columns $ fromList [((5,3), True), ((10,6), True)]
--  [5,6,7,8,9,10]
-- -}
-- columns :: SpreadSheet a -> [Natural]
-- columns Empty = []
-- columns (Mk t b _) = Ix.range (Index.col t, Index.col b)

{- | Enumera las filas de la hoja de cálculo.

 >>> rows empty
 []

 >>> rows $ fromList [((5,3), True), ((10,6), True)]
 [3,4,5,6]
-}

-- rows :: SpreadSheet a -> [Natural]
-- rows Empty = []
-- rows (Mk t b _) = Ix.range (Index.row t, Index.row b)

{- Constructures -}

{- | Hoja de cálculo vacía.

 >>> empty == fromList []
 True
-}
empty :: SpreadSheet a
empty = mempty

{- | A partir de un solo valor, se genera la hoja que en la posición
 (1,1) tiene dicho valor.

 >>> singleton True
 Range (1,1) (1,1)
 fromList [((1,1), True)]
-}
singleton :: a -> SpreadSheet a
singleton = singletonPos (index 1 1)

{- | Generar una hoja con un valor en una posición.

 >>> singletonPos (1,5) True
 Range (1,5) (1,5)
 fromList [((1,5),True)]
-}
singletonPos :: Index -> a -> SpreadSheet a
singletonPos p v = fromList [(p, v)]

{- | De una lista de pares posición-valor se genera la hoja
 correspondiente. En el caso de tener posiciones repetidas,
 permanecerá la que tenga un índice mayor.

 >>> fromList [((1,1), 0), ((1,2), 1), ((1,1), 5)]
 Range (1,1) (1,2)
 fromList [((1,1),5),((1,2),3)]
-}
fromList :: [(Index, a)] -> SpreadSheet a
fromList = fromDict . Map.fromList

{- | Construcción de forma equivalente a 'fromList', salvo que se usa un
 diccionario de la librearía 'Data.Map.Map'.

 Destinado a construir la hoja de cálculo aprovechando las funciones
 optimizadas para diccionarios. Las precondiciones de las funciones
 de construcción de diccionarios no se comprueban, ya que forman
 parte de otro módulo.

 >>> let cells = zipWith (,) [(x,y) | x <- [1..9], y <- [1..9]] [1..]
 >>> Map.valid $ Map.fromAscList cells
 True
 >>> Map.valid $ Map.fromAscList (reverse cells)
 False
 >>> fromDict $ Map.fromAscList cells
 Range (1,1) (9,9)
 fromList [((1,1),1),((1,2),2),((1,3),3),((1,4),...
-}
fromDict :: Map.Map Index a -> SpreadSheet a
fromDict = SpreadSheet

{- | Transforma una hoja de cálculo a una lista que contiene sus
 valores.

 >>> toListValues empty == []
 True

 La función 'Data.Foldable.toList' aplicada a esta esctructura de
 datos utiliza esta función.
-}
toListValues :: SpreadSheet a -> [a]
toListValues = map snd . toListPosValues

{- | Transforma una hoja de cálculo a una lista que contiene sus
 valores junto a las posiciones en las que están.
-}
toListPosValues :: SpreadSheet a -> [(Index, a)]
toListPosValues = Map.toList . getSpreadSheet

{- Update/Modify -}

union :: SpreadSheet a -> SpreadSheet a -> SpreadSheet a
union (SpreadSheet s1) (SpreadSheet s2) = fromDict $ Map.union s1 s2

{- | Genera una hoja de cálculo nueva con la intersección de las
 celdas, dejando los valores de la primera hoja.
-}
intersection :: SpreadSheet a -> SpreadSheet a -> SpreadSheet a
intersection (SpreadSheet s1) (SpreadSheet s2) = fromDict $ Map.intersection s1 s2

-- | Operador para la intersección. Ver 'intersection'.
(</>) :: SpreadSheet a -> SpreadSheet a -> SpreadSheet a
(</>) = intersection

{- | Inserta en una posición un valor. Este valor se puede generar
 según una función sobre una hoja de cálculo.

 Esta función permite la composición de sucesivas inserciones.

 >>> empty & put (1,1) (const True) & put (1,2) (const False)
 Range (1,1) (1,2)
 fromList [((1,1),True),((1,2),False)]
-}
put :: Index -> (SpreadSheet a -> a) -> SpreadSheet a -> SpreadSheet a
put idx f s = fromDict $ Map.insert idx (f s) (getSpreadSheet s)

{- Seleccionar un rango de una SpreadSheet

Permite definir rangos fijos, como por ejemplo:

a3a4 = range Range (1,3) (1,4) :: Spreadsheet -> Spreadsheet --

-}

{- | Obtiene el valor de una posición ('Pos') de una hoja de cálculo.

 Si la celda contiene un dato, devuelve el valor como ('Just' @valor@). Si no existe, devuelve 'Nothing'.

 >>> get (1,1) $ fromList [((1,1), "valor")]
 Just "valor"

 >>> get (2,2) $ fromList [((1,1), "valor")]
 Nothing
-}
get :: Index -> SpreadSheet a -> Maybe a
get idx (SpreadSheet s) = Map.lookup idx s

{- | Genera una hoja de cálculo a partir de una dada según una función
 discriminante sobre las posiciónes.

 En el siguiente ejemplo se eligen por ejemplo las celdas presentes en la diagonal.

 >>> select (\(col,row) -> col == row) $ fromList [((1,1), "a"), ((2,1), "b"), ((1,2), "c"), ((2,2), "d")]
 Range (1,1) (2,2)
 fromList [((1,1),"a"),((2,2),"d")]
-}
select :: (Index -> Bool) -> SpreadSheet a -> SpreadSheet a
select f (SpreadSheet s) = fromDict $ fst $ Map.partitionWithKey (\k _ -> f k) s

{- | Dando un rango y una hoja de cálculo, genera una hoja de cálculo
 con las celdas en dicho rango.

 >>> let cells = zipWith (,) [(x,y) | x <- [1..9], y <- [1..9]] [1..]
 >>> range (Range (8,7) (9,9)) $ fromList cells
 Range (8,7) (9,9)
 fromList [((8,7),70),((8,8),71),((8,9),72),((9,7),79),((9,8),80),((9,9),81)]
-}
selectRange :: (Index, Index) -> SpreadSheet a -> SpreadSheet a
selectRange (idxA, idxB) s
    | Index.col idxA >= Col 0
        && Index.row idxA >= Row 0
        && Index.col idxA <= Index.col idxB
        && Index.row idxA <= Index.row idxB =
        go
    | otherwise = mempty
  where
    go =
        select
            ( \i ->
                Index.col idxA <= Index.col i
                    && Index.row idxA <= Index.row i
                    && Index.col idxB >= Index.col i
                    && Index.row idxB >= Index.row i
            )
            s

{- | Selecciona las celdas con datos de una hoja de cálculo para una
 columna dada.

 >>> let cells = zipWith (,) [(x,y) | x <- [1..5], y <- [1..5]] [1..]
 >>> column 2 $ fromList cells
 Range (2,1) (2,5)
 fromList [((2,1),6),((2,2),7),((2,3),8),((2,4),9),((2,5),10)]
-}
column :: Natural -> SpreadSheet a -> SpreadSheet a
column n = select (\i -> Index.row i == Row n)

{- | Selecciona las celdas con datos de una hoja de cálculo para una
 fila dada.

 >>> let cells = zipWith (,) [(x,y) | x <- [1..5], y <- [1..5]] [1..]
 >>> row 3 $ fromList cells
 Range (1,3) (5,3)
 fromList [((1,3),3),((2,3),8),((3,3),13),((4,3),18),((5,3),23)]
-}
row :: Natural -> SpreadSheet a -> SpreadSheet a
row n = select (\i -> Index.row i == Row n)

--

{- | La función 'mapMaybe' está orientada a la selección de valores de
 un tipo que puedan ser transformados a otro. Tomando una hoja de
 cálculo se obtiene una nueva, con un tipo distinto, con solo
 aquellos valores que han podido ser extraidos.

 Esta función tiene su ejemplo en el conjunto de funciónes
 'Data.SpreadSheet.Cell.extractDouble',
 'Data.SpreadSheet.Cell.extractString' o
 'Data.SpreadSheet.Cell.extractDay' del módulo 'Data.SpreadSheet.Cell.Cell'.

 >>> let days = fromList [((1,1), "2018-12-23"), ((1,2),"-"), ((1,3),"2018-12-25")] :: SpreadSheet String
 >>> let parseDay = (\x -> parseTimeM True defaultTimeLocale "%Y-%-m-%-d" x) :: String -> Maybe Day
 >>> :t mapMaybe parseDay days
 extract p days :: SpreadSheet Day
 >>> mapMaybe parseDay days
 Range (1,1) (1,3)
 fromList [((1,1),2018-12-23),((1,3),2018-12-25)]
-}
mapMaybe :: (a -> Maybe b) -> SpreadSheet a -> SpreadSheet b
mapMaybe f (SpreadSheet s) = fromDict $ Map.mapMaybe f s

-- zipWith :: (a -> b -> c) -> SpreadSheet a -> SpreadSheet b -> SpreadSheet c
