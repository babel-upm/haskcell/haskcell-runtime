-- | This module contains the definition of the Dependency data type.
module Data.Dependency where

-- external imports
import Algebra.Graph.AdjacencyMap (AdjacencyMap, empty)
import Data.Aeson (FromJSON, FromJSONKey, ToJSON, ToJSONKey)
import GHC.Generics ()

-- internal imports
import Data.Cell (Index)

-- | 'Dependencies' will be stored in a directed graph.
type Dependencies = AdjacencyMap Index

-- | The 'empty' value of an empty dependency graph.
empty :: Dependencies
empty = Algebra.Graph.AdjacencyMap.empty

instance (ToJSON a, ToJSONKey a) => ToJSON (AdjacencyMap a)

instance (Ord a, FromJSON a, FromJSONKey a) => FromJSON (AdjacencyMap a)
