{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

{- | This module contains functions to deal with indices and references
    in spread sheets.
-}
module Data.Index where

-- external imports
import Data.Char (chr, ord)
import Data.Ix (Ix)
import GHC.Natural (naturalFromInteger, naturalToInteger)
import Text.Read (readMaybe)
import Text.Regex (matchRegexAll)

-- internal imports
import Data.Aeson (FromJSON, FromJSONKey, ToJSON, ToJSONKey)
import Data.Aeson.Types ()
import Data.Parsing (columnRegex, rowRegex)
import GHC.Generics (Generic)
import GHC.Num (Natural)

newtype Col = Col
    { _col :: Natural
    }
    deriving
        ( Show
        , Eq
        , Ord
        , Num
        , Enum
        , Generic
        , Read
        , ToJSON
        , FromJSON
        , ToJSONKey
        , FromJSONKey
        , Ix
        )

newtype Row = Row
    { _row :: Natural
    }
    deriving
        ( Show
        , Eq
        , Ord
        , Num
        , Enum
        , Generic
        , Read
        , ToJSON
        , FromJSON
        , ToJSONKey
        , FromJSONKey
        , Ix
        )

newtype Index = Index
    { _index :: (Col, Row)
    }
    deriving
        ( Show
        , Eq
        , Ord
        , Generic
        , ToJSON
        , FromJSON
        , ToJSONKey
        , FromJSONKey
        , Ix
        )

row :: Index -> Row
row (Index (_, r)) = r

col :: Index -> Col
col (Index (c, _)) = c

index :: Col -> Row -> Index
index c r = Index (c, r)

rowNat :: Index -> Natural
rowNat (Index (_, Row r)) = r

colNat :: Index -> Natural
colNat (Index (Col c, _)) = c

indexNat :: Natural -> Natural -> Index
indexNat c r = Index (Col c, Row r)

-- | 'rowColToInt' takes a row and a column and returns an index if succeeds.
rowColToInt :: (String, String) -> Maybe Index
rowColToInt (r, c) = do
    r' <- rowToInt r
    c' <- colToInt c
    return $ index (Col c') (Row r')

{- | 'rowToInt' takes a @String@ and returns a row index value if
 succeeds.
-}
rowToInt :: String -> Maybe Natural
rowToInt = readMaybe

{- | 'colToInt' takes a String and returns a column index value if
 succeeds.
-}
colToInt :: String -> Maybe Natural -- TODO now it does not work with AA AAA...
colToInt (x : _) = Just $ naturalFromInteger $ fromIntegral $ ord x - 65 -- unsafe
colToInt _ = Nothing

{- | 'intToRow' takes an index row value and returns a row
 reference.
-}
intToRow :: Natural -> String
intToRow = show

-- | 'intToCol' takes an index column and returns a column reference.
intToCol :: Natural -> String -- TODO fix for greater than 27
intToCol c = [chr (fromIntegral $ naturalToInteger $ c + 65)]

-- | 'indexToRef'
indexToRef :: Index -> String
indexToRef i = intToCol (colNat i) <> intToRow (rowNat i)

{- | 'obtainRowCol' obtains the row and the column from an input reference. It returns @Nothing@ if it
 can not match the row or the column.
-}
obtainRowCol :: String -> Maybe (String, String)
obtainRowCol xs = do
    r <- matchRegexAll rowRegex xs
    c <- matchRegexAll columnRegex xs
    return (snd' r, snd' c)
  where
    snd' (_, x, _, _) = x
