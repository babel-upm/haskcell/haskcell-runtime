{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

-- | This module contains the definition of the ExternalModule data type and some useful instances.
module Data.Module where

-- external imports
import Data.Aeson (
    FromJSON (parseJSON),
    KeyValue ((.=)),
    ToJSON (toJSON),
    object,
    withObject,
    (.:),
 )
import Data.String (IsString)
import Data.Text (Text)
import GHC.Generics (Generic)

newtype ModuleContent = ModuleContent Text deriving (Show, Eq, Generic, IsString, Semigroup, Monoid)
newtype ModuleName = ModuleName Text deriving (Show, Eq, Generic, IsString, Semigroup, Monoid)
data Module = Module
    { getModuleName :: ModuleName
    , getModuleContent :: ModuleContent
    }
    deriving (Show, Generic)

instance FromJSON Module where
    parseJSON = withObject "externalModule" $ \o -> do
        text <- o .: "text"
        name <- o .: "name"
        return $ Module (ModuleName name) (ModuleContent text)

instance ToJSON Module where
    toJSON (Module (ModuleName name) (ModuleContent content)) =
        object
            [ "module_name" .= name
            , "text" .= content
            ]
