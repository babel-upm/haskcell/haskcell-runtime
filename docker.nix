{ pkgs ? import <nixpkgs> { }
, pkgsLinux ? import <nixpkgs> { system = "x86_64-linux"; }
}:
let
  pkgsLocal = import ./default.nix;
 in
pkgs.dockerTools.buildImage {
  name = "haskcell-runtime";
  tag = "latest";
  fromImage = pkgs.dockerTools.pullImage {
    imageName = "alpine";
    imageDigest = "sha256:a777c9c66ba177ccfea23f2a216ff6721e78a662cd17019488c417135299cd89";
    sha256 = "OTxD2mJThfcIxIoRlrX388RijSAVv9EIedVy1P8dtpc=";
  };
  contents =
    [
      pkgs.bash
      pkgs.coreutils-full
      pkgsLocal.shell.ghc
      pkgsLocal.haskcell-runtime.components.exes.haskcell-runtime-server
      ./src/TH/Defaults
      ];
  created = "now";
  extraCommands = ''
    touch CellDefinitions.hs
    touch ExternalModule.hs
    chmod 777 CellDefinitions.hs ExternalModule.hs
    mkdir -p src/TH/Defaults
    mv Arguments.hs Expressions.hs Signatures.hs src/TH/Defaults/
    '';
  config = {
    ExposedPorts = {
      "9160" = {};
    };
    Env = [
      "NIX_GHC_LIBDIR=${pkgsLocal.shell.NIX_GHC_LIBDIR}"
    ];
    Cmd = [ "/bin/haskcell-runtime-server" ];
  };
}
